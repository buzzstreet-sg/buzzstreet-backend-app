
## Local Development Setup

For the step to set up the local development environment, please refer to the README in the main parent repository.

Do note that this application is dependent on the environment setup (PostgreSQL and Redis) to run correct.

After you have successfully started the environment (virtual machine), 
you can run this application with the steps and commands below.

## Installation

Add the `.env` file, these configuration are important for running the application on the local development environment.

```dotenv

## Application
PORT=3001

## Frontend application
BS_FRONTEND_APP_URL=http://localhost:3000

## Logging
LOG_LEVEL=debug
LOG_DIR=./logs
LOG_MAX_SIZE=20m
LOG_MAX_FILES=14d

## Redis
REDIS_HOST=172.31.22.155
REDIS_PORT=6379
REDIS_TRAFFIC_INTERVAL_MIN=5
REDIS_TRAFFIC_TTL_SEC=3600
REDIS_WEATHER_INTERVAL_MIN=30
REDIS_WEATHER_TTL_SEC=3600

## Postgres
DB_HOST=172.31.22.155
DB_PORT=5432
DB_USERNAME=psqladmin
DB_PASSWORD=password
DB_NAME=db_buzzstreet
DB_SCHEMA=schema_buzzstreet

## User search transaction history
RECENT_SEARCHES_LIMIT=10
TOP_SEARCHES_LIMIT=10
```

Run the command.

```bash
$ npm install
```

## Running the app

Application will run on `http://localhost:3001/`

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```