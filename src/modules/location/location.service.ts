import { BadRequestException, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { RedisService} from "../../common/redis/redis.service";
import { MyLogger} from "../../common/logger/logger.service";
import { roundDownToNearestMinutes } from "../../common/utils/date-utils";

@Injectable()
export class LocationService {
  private readonly logger = new MyLogger(LocationService.name);
  constructor(
      private readonly redisService: RedisService,
      private readonly configService: ConfigService
  ) {}

  /**
   * Finds locations by a given date and time string.
   *
   * @param {string} dateTimeStr - A string representing a date and time.
   * @returns {Promise<string[]>} - An array of location strings.
   * @throws {BadRequestException} - If the date format is invalid.
   */
  async findLocationByDateTime(dateTimeStr: string): Promise<string[]> {

    const dateTime = new Date(dateTimeStr);
    if (isNaN(dateTime.getTime())) {
      throw new BadRequestException(`Invalid date format: ${dateTimeStr}`);
    }

    // Round down the time to search.
    const REDIS_TRAFFIC_INTERVAL_MIN =
        this.configService.get<number>('REDIS_TRAFFIC_INTERVAL_MIN', 5);
    const roundedDateTime = roundDownToNearestMinutes(dateTime, REDIS_TRAFFIC_INTERVAL_MIN);
    this.logger.log(`Converted ${dateTimeStr} to ${roundedDateTime.toISOString()}`);

    const key = `locations:${roundedDateTime.getTime()}`;
    const value = await this.redisService.getValue(key);

    if (value) {
      this.logger.debug(`${roundedDateTime.toISOString()} returned ${value}.`);
      return JSON.parse(value);
    } else {
      this.logger.log(`No value found for ${roundedDateTime.toISOString()}`);
      return [];
    }
  }
}
