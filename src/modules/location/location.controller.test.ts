import { Test, TestingModule } from '@nestjs/testing';
import { LocationController } from './location.controller';
import { LocationService } from './location.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { RedisService } from '../../common/redis/redis.service';
import { ApiResponse } from '../../common/response.model';
import { BadRequestException } from '@nestjs/common';
import Redis from 'ioredis'; // Import ioredis

describe('LocationController', () => {
  let locationController: LocationController;
  let locationService: LocationService;

  beforeEach(async () => {
    const configServiceMock = {
      get: jest.fn().mockReturnValue(10),
    };

    const redisClientMock = new Redis(); // Use ioredis as a mock Redis client

    const module: TestingModule = await Test.createTestingModule({
      controllers: [LocationController],
      providers: [
        LocationService,
        RedisService,
        {
          provide: ConfigService,
          useValue: configServiceMock,
        },
        {
          provide: 'REDIS_CLIENT', // Provide the 'REDIS_CLIENT' token
          useValue: redisClientMock, // Use ioredis as the mock Redis client here
        },
      ],
    }).compile();

    locationController = module.get<LocationController>(LocationController);
    locationService = module.get<LocationService>(LocationService);
  });

  describe('getByTimestamp', () => {
    it('should return ApiResponse with locations for a valid dateTime', async () => {
      // Arrange
      const dateTimeStr = '2023-12-16T12:34:56';
      const expectedLocations = ['Location1', 'Location2'];

      // Mocking the locationService to return expected data
      jest.spyOn(locationService, 'findLocationByDateTime').mockResolvedValue(expectedLocations);

      // Act
      const result = await locationController.getByTimestamp(dateTimeStr);

      // Assert
      expect(result).toEqual(new ApiResponse<string[]>(expectedLocations));
    });

    it('should throw BadRequestException for an invalid dateTime format', async () => {
      // Arrange
      const dateTimeStr = 'InvalidDateTime';

      // Mocking the locationService to throw BadRequestException
      jest.spyOn(locationService, 'findLocationByDateTime').mockRejectedValue(new BadRequestException());

      // Act & Assert
      await expect(locationController.getByTimestamp(dateTimeStr)).rejects.toThrowError(
          BadRequestException,
      );
    });
  });
});
