import { Controller, Get, Query } from '@nestjs/common';
import { LocationService } from './location.service';
import { MyLogger } from "../../common/logger/logger.service";
import { ApiResponse } from "../../common/response.model";

@Controller('locations')
export class LocationController {
  private readonly logger = new MyLogger(LocationController.name);

  constructor(private readonly locationService: LocationService) {}

  /**
   * Get locations by timestamp.
   *
   * @param {string} dateTimeStr - The date and time in string format.
   * @returns {Promise<ApiResponse<string[]>>} A ApiResponse containing an array of location strings.
   * @throws {BadRequestException} If the dateTimeStr is in an invalid format.
   */
  @Get()
  async getByTimestamp(@Query('dateTime') dateTimeStr: string): Promise<ApiResponse<string[]>> {
    this.logger.log(`Request received with dateTime: ${dateTimeStr}`);

    const locations = await this.locationService.findLocationByDateTime(dateTimeStr);

    return new ApiResponse<string[]>(locations);
  }
}
