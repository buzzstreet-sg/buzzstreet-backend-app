import { LocationService } from './location.service';
import { BadRequestException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { RedisService } from '../../common/redis/redis.service';
import Redis from 'ioredis';

describe('LocationService', () => {
  let locationService: LocationService;
  let configService: ConfigService;
  let redisService: RedisService;

  beforeEach(() => {
    const redisClient = new Redis();
    redisService = new RedisService(redisClient);
    configService = new ConfigService();
    locationService = new LocationService(redisService, configService);
  });

  describe('findLocationByDateTime', () => {

    it('should return an array of locations for a valid dateTime if key exist', async () => {
      const dateTimeStr = '2023-12-16T12:34:56';
      const expectedLocations = ['Location1', 'Location2', 'Location3'];

      jest.spyOn(configService, 'get').mockReturnValue(5);
      jest.spyOn(redisService, 'getValue').mockResolvedValue(JSON.stringify(expectedLocations));

      const result = await locationService.findLocationByDateTime(dateTimeStr);

      expect(result).toEqual(expectedLocations);
    });

    it('should return an empty array of locations for a valid dateTime if key does not exist', async () => {
      const dateTimeStr = '2023-12-16T12:34:56';
      const expectedLocations = [];

      jest.spyOn(configService, 'get').mockReturnValue(5);
      jest.spyOn(redisService, 'getValue').mockResolvedValue(null);

      const result = await locationService.findLocationByDateTime(dateTimeStr);

      expect(result).toEqual(expectedLocations);
    });

    it('should throw BadRequestException for an invalid dateTime format', async () => {
      // Arrange
      const dateTimeStr = 'InvalidDateTime';

      // Act & Assert
      await expect(locationService.findLocationByDateTime(dateTimeStr)).rejects.toThrowError(
          BadRequestException,
      );
    });
  });
});
