import { WeatherService } from './weather.service';
import { ConfigService } from '@nestjs/config';
import { RedisService } from '../../common/redis/redis.service';
import { BadRequestException } from '@nestjs/common';
import Redis from 'ioredis';

describe('WeatherService', () => {
  let weatherService: WeatherService;
  let configService: ConfigService;
  let redisService: RedisService;

  beforeEach(() => {
    const redisClient = new Redis();
    redisService = new RedisService(redisClient);
    configService = new ConfigService();
    weatherService = new WeatherService(redisService, configService);
  });

  describe('findForecastByLocationAndDateTime', () => {

    it('should return a weather forecast for a valid location and dateTime', async () => {
      const location = 'TestLocation';
      const dateTimeStr = '2023-12-16T12:34:56';
      const expectedForecast = 'Sunny';

      jest.spyOn(configService, 'get').mockReturnValue(5);
      jest.spyOn(redisService, 'getValue').mockResolvedValue(expectedForecast);

      const result = await weatherService.findForecastByLocationAndDateTime(location, dateTimeStr);

      expect(result).toEqual({
        location: location,
        dateTime: dateTimeStr,
        forecast: expectedForecast,
      });
    });

    it('should return null for a valid location and dateTime if key does not exist', async () => {
      const location = 'TestLocation';
      const dateTimeStr = '2023-12-16T12:34:56';

      jest.spyOn(configService, 'get').mockReturnValue(5);
      jest.spyOn(redisService, 'getValue').mockResolvedValue(null);

      const result = await weatherService.findForecastByLocationAndDateTime(location, dateTimeStr);

      expect(result).toEqual({
        location: location,
        dateTime: dateTimeStr,
        forecast: null,
      });
    });

    it('should throw BadRequestException for an empty location', async () => {
      const location = ''; // Empty location
      const dateTimeStr = '2023-12-16T12:34:56';

      await expect(weatherService.findForecastByLocationAndDateTime(location, dateTimeStr)).rejects.toThrowError(
          BadRequestException,
      );
    });

    it('should throw BadRequestException for an invalid dateTime format', async () => {
      // Arrange
      const location = 'TestLocation';
      const dateTimeStr = 'InvalidDateTime';

      await expect(weatherService.findForecastByLocationAndDateTime(location, dateTimeStr)).rejects.toThrowError(
          BadRequestException,
      );
    });
  });
});
