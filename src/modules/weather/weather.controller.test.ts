import { Test, TestingModule } from '@nestjs/testing';
import { WeatherController } from './weather.controller';
import { WeatherService, WeatherForecast } from './weather.service';
import { ApiResponse } from '../../common/response.model';
import { BadRequestException } from '@nestjs/common';

describe('WeatherController', () => {
  let weatherController: WeatherController;
  let weatherService: WeatherService;

  beforeEach(async () => {
    // Mock the WeatherService and its methods
    const weatherServiceMock = {
      findForecastByLocationAndDateTime: jest.fn(),
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [WeatherController],
      providers: [
        {
          provide: WeatherService,
          useValue: weatherServiceMock,
        },
      ],
    }).compile();

    weatherController = module.get<WeatherController>(WeatherController);
    weatherService = module.get<WeatherService>(WeatherService);
  });

  describe('getForecastByLocationAndTimestamp', () => {

    it('should return ApiResponse with weather forecast for valid location and timestamp', async () => {
      const location = 'TestLocation';
      const dateTimeStr = '2023-12-16T12:34:56';
      const expectedWeatherForecast: WeatherForecast = {
        location: 'TestLocation',
        dateTime: '2023-12-16T12:34:56',
        forecast: 'Sunny',
      };

      jest.spyOn(weatherService, 'findForecastByLocationAndDateTime')
      .mockResolvedValue(expectedWeatherForecast);

      const result = await weatherController.getForecastByLocationAndTimestamp(location, dateTimeStr);

      expect(result).toEqual(new ApiResponse<WeatherForecast>(expectedWeatherForecast));
    });

    it('should throw BadRequestException for an invalid dateTime format', async () => {
      const location = 'TestLocation';
      const dateTimeStr = 'InvalidDateTime';

      jest.spyOn(weatherService, 'findForecastByLocationAndDateTime')
      .mockRejectedValue(new BadRequestException());

      await expect(weatherController.getForecastByLocationAndTimestamp(location, dateTimeStr)).rejects.toThrowError(
          BadRequestException,
      );
    });

    it('should throw BadRequestException for an empty location', async () => {
      const location = ''; // Empty location
      const dateTimeStr = '2023-12-16T12:34:56';

      jest.spyOn(weatherService, 'findForecastByLocationAndDateTime')
      .mockRejectedValue(new BadRequestException());

      await expect(weatherController.getForecastByLocationAndTimestamp(location, dateTimeStr)).rejects.toThrowError(
          BadRequestException,
      );
    });
  });
});
