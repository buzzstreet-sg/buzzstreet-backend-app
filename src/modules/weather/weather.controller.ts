import { Controller, Get, Query, Logger } from '@nestjs/common';
import { WeatherService, WeatherForecast } from "./weather.service";
import { MyLogger } from "../../common/logger/logger.service";
import { ApiResponse } from "../../common/response.model";

@Controller('weather')
export class WeatherController {
  private readonly logger = new MyLogger(WeatherController.name);

  constructor(private readonly weatherService: WeatherService) {}

  /**
   * Retrieves a weather forecast for a specific location and timestamp.
   *
   * @param {string} location - The location for which to retrieve the weather forecast.
   * @param {string} dateTimeStr - A string representing the date and time.
   * @returns {Promise<ApiResponse<WeatherForecast>>} - An ApiResponse containing the weather forecast.
   */
  @Get()
  async getForecastByLocationAndTimestamp(
      @Query('location') location: string,
      @Query('dateTime') dateTimeStr: string
  ): Promise<ApiResponse<WeatherForecast>> {
    this.logger.log(`Request received with dateTime: ${dateTimeStr} and location: ${location}.`);

    const forecast =
        await this.weatherService.findForecastByLocationAndDateTime(location, dateTimeStr);

    return new ApiResponse<WeatherForecast>(forecast);
  }
}
