import { BadRequestException, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { RedisService } from "../../common/redis/redis.service";
import { MyLogger} from "../../common/logger/logger.service";
import { roundDownToNearestMinutes } from "../../common/utils/date-utils";

export interface WeatherForecast {
  location: string;
  dateTime: string;
  forecast: string | null;
}

@Injectable()
export class WeatherService {
  private readonly logger = new MyLogger(WeatherService.name);

  constructor(
      private readonly redisService: RedisService,
      private readonly configService: ConfigService
  ) {}

  /**
   * Retrieves a weather forecast for a specific location and timestamp.
   *
   * @param {string} location - The location for which to retrieve the weather forecast.
   * @param {string} dateTimeStr - A string representing the date and time.
   * @returns {Promise<WeatherForecast>} - The weather forecast data.
   * @throws {BadRequestException} If the location is empty or if the date format is invalid.
   */
  async findForecastByLocationAndDateTime(location: string, dateTimeStr: string): Promise<WeatherForecast> {

    const strippedLocation =
        location.replace(/\s+/g, '').toLowerCase();

    if (!strippedLocation) {
      throw new BadRequestException(`Location cannot be empty: "${location}".`);
    }

    const dateTime = new Date(dateTimeStr);
    if (isNaN(dateTime.getTime())) {
      throw new BadRequestException(`Invalid date format: ${dateTimeStr}`);
    }

    const REDIS_WEATHER_INTERVAL_MIN =
        this.configService.get<number>('REDIS_WEATHER_INTERVAL_MIN', 10);
    const roundedDateTime = roundDownToNearestMinutes(dateTime, REDIS_WEATHER_INTERVAL_MIN);

    const key = `weather:${strippedLocation}:${roundedDateTime.getTime()}`;
    const value = await this.redisService.getValue(key);

    if (value) {
      this.logger.debug(`${roundedDateTime.toISOString()} returned ${value}.`);
    } else {
      this.logger.log(`No value found for ${roundedDateTime.toISOString()}`);
    }

    return {
      location: location,
      dateTime: dateTimeStr,
      forecast: value,
    };

  }
}
