import { Controller, Body, Query, Post, Get } from '@nestjs/common';
import { SearchHistoryService } from './search.service';
import { ApiResponse } from "../../common/response.model";
import { SearchHistoryDto } from "./dto/search.base.dto";
import { MyLogger } from "../../common/logger/logger.service";

@Controller('search')
export class SearchHistoryController {
  private readonly logger = new MyLogger(SearchHistoryController.name);

  constructor(private searchHistoryService: SearchHistoryService) {}

  /**
   * Create a new search history entry.
   *
   * @param {{ location: string; dateTime: string }} body - The request body containing location and dateTime.
   * @returns {Promise<ApiResponse<SearchHistoryDto>>} - An ApiResponse containing the created search history entry.
   */
  @Post()
  async create(@Body() body: { location: string; dateTime: string }) {
    this.logger.log(
        `Request received with dateTime: ${body.dateTime} and location: ${body.location}.`
    );

    const searchHistoryDto =
        await this.searchHistoryService.create(body.location, body.dateTime);

    return new ApiResponse<SearchHistoryDto>(searchHistoryDto);
  }


  /**
   * Get the most recent search history entries.
   *
   * @returns {Promise<ApiResponse<SearchHistoryDto[]>>} - An ApiResponse containing the most recent search history entries.
   */
  @Get('recent')
  async getMostRecentSearches() {
    this.logger.log("Request received to get most recent search.");

    const recentSearchHistoryDtos =
        await this.searchHistoryService.getMostRecentSearches();

    return new ApiResponse<SearchHistoryDto[]>(recentSearchHistoryDtos);
  }


  /**
   * Get the top search history entries within a specified period.
   *
   * @param {string} start - The start timestamp of the period.
   * @param {string} end - The end timestamp of the period.
   * @returns {Promise<ApiResponse<SearchHistoryDto[]>>} - An ApiResponse containing the top search history entries within the period.
   */
  @Get('top-within-period')
  async getTopSearchesWithinPeriod(
      @Query('start') start: string,
      @Query('end') end: string
  ): Promise<ApiResponse<SearchHistoryDto[]>> {
    this.logger.log("Request received to get most top search.");

    const topSearches =
        await this.searchHistoryService.findTopSearchesWithinPeriod(start, end);

    return new ApiResponse<SearchHistoryDto[]>(topSearches);
  }


  /**
   * Get the start of the busiest search period within a specified range and period length.
   *
   * @param {string} start - The start timestamp for the search range.
   * @param {string} end - The end timestamp for the search range.
   * @param {string} period - The length of the search period.
   * @returns {Promise<ApiResponse<string>>} - An ApiResponse containing the start timestamp of the busiest period.
   */
  @Get('busiest-period')
  async getBusiestSearchPeriod(
      @Query('start') start: string,
      @Query('end') end: string,
      @Query('period') period: string
  ): Promise<ApiResponse<string>> {
    this.logger.log(
        "Request received to get start of busiest period within defined start and end."
    );

    const earliestTimestamp =
        await this.searchHistoryService.findStartOfBusiestPeriod(start, end, period);

    return new ApiResponse<string>(earliestTimestamp);
  }
}
