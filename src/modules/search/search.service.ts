import { BadRequestException, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { performance } from 'perf_hooks';
import { SearchHistory } from './search.entity';
import { SearchHistoryDto } from "./dto/search.base.dto";
import { MyLogger } from "../../common/logger/logger.service";

@Injectable()
export class SearchHistoryService {
  private readonly logger = new MyLogger(SearchHistoryService.name);

  constructor(
      @InjectRepository(SearchHistory)
      private searchHistoryRepository: Repository<SearchHistory>,
      private configService: ConfigService,
  ) {}

  /**
   * Create a new search history entry.
   *
   * @param {string} searchForLocation - The location for the search.
   * @param {string} searchForDateTime - A string representing the date and time of the search.
   * @returns {Promise<SearchHistoryDto>} - A DTO containing the created search history entry.
   * @throws {BadRequestException} If the location is empty or if the date format is invalid.
   */
  async create(searchForLocation: string, searchForDateTime: string): Promise<SearchHistoryDto> {
    if (!searchForLocation.trim()) {
      throw new BadRequestException(`Invalid location: ${searchForLocation}`);
    }

    const searchForTimestamp = new Date(searchForDateTime);
    if (isNaN(searchForTimestamp.getTime())) {
      throw new BadRequestException(`Invalid date format: ${searchForDateTime}`);
    }

    const searchHistory =
        this.searchHistoryRepository.create({
          searchForLocation: searchForLocation,
          searchForTimestamp: searchForTimestamp
        });

    const savedSearchHistory =
        await this.searchHistoryRepository.save(searchHistory);

    this.logger.log(`Saved search history record: ${savedSearchHistory.toString()}`);

    return new SearchHistoryDto(savedSearchHistory);
  }

  /**
   * Get the most recent search history entries.
   *
   * @returns {Promise<SearchHistoryDto[]>} - An array of DTOs containing the most recent search history entries.
   */
  async getMostRecentSearches(): Promise<SearchHistoryDto[]> {
    const limit = this.configService.get<number>('RECENT_SEARCHES_LIMIT');

    const startTime = performance.now();
    this.logger.log(`Start query for most recent search history at ${startTime}`);

    const searchHistories =  await this.searchHistoryRepository.find({
      order: {
        created_at: 'DESC'
      },
      take: limit
    });

    const endTime = performance.now();
    const queryTime = endTime - startTime;
    this.logger.log(`Query time for getMostRecentSearches: ${queryTime} ms`);

    this.logger.log(`Query return ${searchHistories.length} record(s).`);

    return searchHistories.map(searchHistory => new SearchHistoryDto(searchHistory));
  }

  /**
   * Find the top search history entries within a specified period.
   *
   * @param {string} start - The start timestamp of the period.
   * @param {string} end - The end timestamp of the period.
   * @returns {Promise<SearchHistoryDto[]>} - An array of DTOs containing the top search history entries within the period.
   * @throws {BadRequestException} If start and end parameters are not provided or if timestamps are invalid.
   */
  async findTopSearchesWithinPeriod(start: string, end: string): Promise<SearchHistoryDto[]> {

    if (!start || !end) {
      throw new BadRequestException('Start and end parameters are required');
    }

    const startTimestamp = parseInt(start);
    const endTimestamp = parseInt(end);

    if (isNaN(startTimestamp) || isNaN(endTimestamp)) {
      throw new BadRequestException('Invalid start or end timestamp');
    }

    const startDate = new Date(startTimestamp);
    const endDate = new Date(endTimestamp);

    const limit = this.configService.get<number>('TOP_SEARCHES_LIMIT');

    const rawQuery = `
        SELECT
            search_for_location as "searchForLocation",
            search_for_timestamp as "searchForTimestamp"
        FROM (
            SELECT
                search_for_location,
                search_for_timestamp,
                COUNT(*) AS count
            FROM schema_buzzstreet.tbl_search_history
            WHERE created_at BETWEEN $1 AND $2
            GROUP BY search_for_location, search_for_timestamp
        ) AS result
        ORDER BY count DESC, search_for_location DESC
        LIMIT $3;
    `;

    const startTime = performance.now();
    this.logger.log(`Start query for top searches at ${startTime}`);

    const searchHistories =
        await this.searchHistoryRepository
            .query(rawQuery, [startDate.toISOString(), endDate.toISOString(), limit]);

    const endTime = performance.now();
    const queryTime = endTime - startTime;
    this.logger.log(`Query time for top searches: ${queryTime} ms`);

    this.logger.log(`Query return ${searchHistories.length} record(s).`);

    return searchHistories.map(searchHistory => new SearchHistoryDto(searchHistory));
  }

  /**
   * Find the start of the busiest search period within a specified range and period length.
   *
   * @param {string} start - The start timestamp for the search range.
   * @param {string} end - The end timestamp for the search range.
   * @param {string} period - The length of the search period (in hours).
   * @returns {Promise<string>} - The start timestamp of the busiest period.
   * @throws {BadRequestException} If start, end, or period parameters are not provided or if values are invalid.
   */
  async findStartOfBusiestPeriod (
      start: string,
      end: string,
      period: string
  ): Promise<string> {

    if (!start || !end || !period) {
      throw new BadRequestException(
          `Start, end and period parameters are required.` +
          `start: "${start}", end: "${end}", period: "${period}"`
      );
    }

    const queryPeriod: number = parseInt(period);
    const endTimestamp = parseInt(end);
    const startTimestamp = parseInt(start);

    if (isNaN(queryPeriod) || isNaN(endTimestamp) || isNaN(startTimestamp)) {
      throw new BadRequestException(
          `Invalid start, end, or period.` +
          `start: "${start}", end: "${end}", period: "${period}"`
      );
    }

    const startDate = new Date(startTimestamp);
    const endDate = new Date(endTimestamp);

    const rawQuery = `
        WITH CountResults AS (
            SELECT
                created_at,
                COUNT(*) OVER (
                    ORDER BY created_at
                    RANGE BETWEEN
                        CURRENT ROW
                        AND $3 FOLLOWING
                    ) AS entry_count
            FROM schema_buzzstreet.tbl_search_history
            WHERE created_at BETWEEN $1 AND $2
        )
        SELECT
            MIN(created_at) as "earliestTimestamp"
        FROM CountResults
        WHERE entry_count = (
            SELECT MAX(entry_count) FROM CountResults
        )
        GROUP BY entry_count
        ORDER BY entry_count DESC
        LIMIT 1;
    `;

    const startTime = performance.now();
    this.logger.log(`Start query for start of busiest period at ${startTime}`);

    const queryResult =
        await this.searchHistoryRepository
            .query(
                rawQuery,
                [
                    startDate.toISOString(),
                  endDate.toISOString(),
                  `'${queryPeriod} hour'`
                ]
            );

    const endTime = performance.now();
    const queryTime = endTime - startTime;
    this.logger.log(`Query time for start of busiest period: ${queryTime} ms`);

    this.logger.log(`Query return ${queryResult.length} record(s).`);

    return queryResult.length ? queryResult[0].earliestTimestamp : '';
  }



}
