import { SearchHistoryService } from "./search.service";
import { BadRequestException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { ConfigService } from '@nestjs/config';
import { SearchHistory } from "./search.entity";
import {SearchHistoryDto} from "./dto/search.base.dto";

describe('SearchHistoryService', () => {
  let searchHistoryService: SearchHistoryService;
  let searchHistoryRepository: Repository<SearchHistory>;
  let configService: ConfigService;

  beforeEach(() => {
    searchHistoryRepository = {
      create: jest.fn(),
      save: jest.fn(),
      find: jest.fn(),
      query: jest.fn(),
    } as unknown as Repository<SearchHistory>;

    configService = {
      get: jest.fn(),
    } as unknown as ConfigService;

    searchHistoryService = new SearchHistoryService(
        searchHistoryRepository,
        configService,
    );
  });

  describe('create', () => {

    it('should create a new search history entry', async () => {
      const searchForLocation = 'Test Location';
      const searchForDateTime = '2023-12-16T12:34:56';
      const expectedSearchHistory: SearchHistory = {
        id: 1,
        searchForLocation,
        searchForTimestamp: new Date(searchForDateTime),
        created_at: new Date()
      };

      jest.spyOn(searchHistoryRepository, 'create').mockReturnValue(expectedSearchHistory);
      jest.spyOn(searchHistoryRepository, 'save').mockResolvedValue(expectedSearchHistory);

      const result = await searchHistoryService.create(searchForLocation, searchForDateTime);

      expect(searchHistoryRepository.create).toHaveBeenCalledWith({
        searchForLocation,
        searchForTimestamp: new Date(searchForDateTime),
      });
      expect(searchHistoryRepository.save).toHaveBeenCalledWith(expectedSearchHistory);
    });

    it('should throw BadRequestException for an empty location', async () => {
      const searchForLocation = ''; // Empty location
      const searchForDateTime = '2023-12-16T12:34:56';

      await expect(
          searchHistoryService.create(searchForLocation, searchForDateTime),
      ).rejects.toThrowError(BadRequestException);
    });

    it('should throw BadRequestException for an invalid date-time format', async () => {

      const searchForLocation = 'Test Location';
      const searchForDateTime = 'InvalidDateTime';

      await expect(
          searchHistoryService.create(searchForLocation, searchForDateTime),
      ).rejects.toThrowError(BadRequestException);
    });
  });

  describe('getMostRecentSearches', () => {

    it('should return an array of most recent search history entries', async () => {
      const limit = 5;
      const searchHistories: SearchHistory[] = [
        { id: 1, searchForLocation: 'Location1', searchForTimestamp: new Date(), created_at: new Date() },
        { id: 2, searchForLocation: 'Location2', searchForTimestamp: new Date(), created_at: new Date() },
        { id: 3, searchForLocation: 'Location3', searchForTimestamp: new Date(), created_at: new Date() },
      ];

      const expectedSearchHistories: SearchHistoryDto[] = [
        { searchForLocation: 'Location1', searchForTimestamp: new Date(), timestamp: new Date() },
        { searchForLocation: 'Location2', searchForTimestamp: new Date(), timestamp: new Date() },
        { searchForLocation: 'Location3', searchForTimestamp: new Date(), timestamp: new Date() },
      ];

      (searchHistoryRepository.find as jest.Mock).mockResolvedValue(searchHistories);

      (configService.get as jest.Mock).mockReturnValue(limit);

      const result = await searchHistoryService.getMostRecentSearches();

      expect(result).toEqual(expectedSearchHistories);
      expect(searchHistoryRepository.find).toHaveBeenCalledWith({
        order: {
          created_at: 'DESC',
        },
        take: limit,
      });
      expect(configService.get).toHaveBeenCalledWith('RECENT_SEARCHES_LIMIT');
    });

    it('should return an empty array if no recent search history entries', async () => {
      const limit = 5;
      const expectedSearchHistories: SearchHistory[] = [];

      (searchHistoryRepository.find as jest.Mock).mockResolvedValue(expectedSearchHistories);

      (configService.get as jest.Mock).mockReturnValue(limit);

      const result = await searchHistoryService.getMostRecentSearches();

      expect(result).toEqual([]);
      expect(searchHistoryRepository.find).toHaveBeenCalledWith({
        order: {
          created_at: 'DESC',
        },
        take: limit,
      });
      expect(configService.get).toHaveBeenCalledWith('RECENT_SEARCHES_LIMIT');
    });
  });

  describe('findTopSearchesWithinPeriod', () => {

    it('should return an array of top search history entries within the specified period', async () => {
      const start = '1641552000000';  // Timestamp for 2022-01-08 00:00:00
      const end = '1641638400000';    // Timestamp for 2022-01-09 00:00:00
      const limit = 5;

      const searchHistories: SearchHistory[] = [
        { id: 1, searchForLocation: 'Location1', searchForTimestamp: new Date('2022-01-08T10:00:00'), created_at: new Date('2022-01-08T10:00:00') },
        { id: 2, searchForLocation: 'Location2', searchForTimestamp: new Date('2022-01-08T14:00:00'), created_at: new Date('2022-01-08T14:00:00') },
        { id: 3, searchForLocation: 'Location3', searchForTimestamp: new Date('2022-01-08T16:00:00'), created_at: new Date('2022-01-08T16:00:00') },
      ];

      const expectedSearchHistories: SearchHistoryDto[] = [
        { searchForLocation: 'Location1', searchForTimestamp: new Date('2022-01-08T10:00:00'), timestamp: new Date('2022-01-08T10:00:00') },
        { searchForLocation: 'Location2', searchForTimestamp: new Date('2022-01-08T14:00:00'), timestamp: new Date('2022-01-08T14:00:00') },
        { searchForLocation: 'Location3', searchForTimestamp: new Date('2022-01-08T16:00:00'), timestamp: new Date('2022-01-08T16:00:00') },
      ];

      (searchHistoryRepository.query as jest.Mock).mockResolvedValue(searchHistories);

      (configService.get as jest.Mock).mockReturnValue(limit);

      const result = await searchHistoryService.findTopSearchesWithinPeriod(start, end);

      expect(result).toEqual(expectedSearchHistories);
    });

    it('should throw BadRequestException if start or end parameters are missing', async () => {
      await expect(searchHistoryService.findTopSearchesWithinPeriod('', '1641638400000')).rejects.toThrowError(
          BadRequestException,
      );
      await expect(searchHistoryService.findTopSearchesWithinPeriod('1641552000000', '')).rejects.toThrowError(
          BadRequestException,
      );
      await expect(searchHistoryService.findTopSearchesWithinPeriod('', '')).rejects.toThrowError(
          BadRequestException,
      );
    });

    it('should throw BadRequestException if start or end parameters are invalid timestamps', async () => {
      await expect(searchHistoryService.findTopSearchesWithinPeriod('InvalidStart', '1641638400000')).rejects.toThrowError(
          BadRequestException,
      );
      await expect(searchHistoryService.findTopSearchesWithinPeriod('1641552000000', 'InvalidEnd')).rejects.toThrowError(
          BadRequestException,
      );
      await expect(searchHistoryService.findTopSearchesWithinPeriod('InvalidStart', 'InvalidEnd')).rejects.toThrowError(
          BadRequestException,
      );
    });
  });

  describe('findStartOfBusiestPeriod', () => {
    it('should return the start timestamp of the busiest period within the specified range and period length', async () => {

      const start = '1641552000000'; // Timestamp for 2022-01-08 00:00:00
      const end = '1641638400000'; // Timestamp for 2022-01-09 00:00:00
      const period = '24'; // 24 hours
      const expectedStartOfBusiestPeriod = '1641600000000'; // Timestamp for 2022-01-08 12:00:00

      (searchHistoryRepository.query as jest.Mock).mockResolvedValue([
        { earliestTimestamp: expectedStartOfBusiestPeriod },
      ]);

      const result = await searchHistoryService.findStartOfBusiestPeriod(start, end, period);

      expect(result).toBe(expectedStartOfBusiestPeriod);
    });

    it('should return an empty string if no result is found', async () => {
      const start = '1641552000000'; // Timestamp for 2022-01-08 00:00:00
      const end = '1641638400000'; // Timestamp for 2022-01-09 00:00:00
      const period = '24'; // 24 hours

      (searchHistoryRepository.query as jest.Mock).mockResolvedValue([]);

      const result = await searchHistoryService.findStartOfBusiestPeriod(start, end, period);

      expect(result).toBe('');
    });

    it('should throw BadRequestException if start, end, or period parameters are missing', async () => {

      await expect(searchHistoryService.findStartOfBusiestPeriod('', '1641638400000', '24')).rejects.toThrowError(
          BadRequestException,
      );
      await expect(searchHistoryService.findStartOfBusiestPeriod('1641552000000', '', '24')).rejects.toThrowError(
          BadRequestException,
      );
      await expect(searchHistoryService.findStartOfBusiestPeriod('1641552000000', '1641638400000', '')).rejects.toThrowError(
          BadRequestException,
      );
    });

    it('should throw BadRequestException if start, end, or period parameters are invalid', async () => {

      await expect(searchHistoryService.findStartOfBusiestPeriod('InvalidStart', '1641638400000', '24')).rejects.toThrowError(
          BadRequestException,
      );
      await expect(searchHistoryService.findStartOfBusiestPeriod('1641552000000', 'InvalidEnd', '24')).rejects.toThrowError(
          BadRequestException,
      );
      await expect(searchHistoryService.findStartOfBusiestPeriod('1641552000000', '1641638400000', 'InvalidPeriod')).rejects.toThrowError(
          BadRequestException,
      );
    });
  });

});
