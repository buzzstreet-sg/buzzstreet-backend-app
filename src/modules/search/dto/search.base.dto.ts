import { SearchHistory } from "../search.entity";

export class SearchHistoryDto {
  searchForLocation: string;
  searchForTimestamp: Date;
  timestamp: Date;

  constructor(searchHistory: SearchHistory) {
    this.searchForLocation = searchHistory.searchForLocation;
    this.searchForTimestamp = searchHistory.searchForTimestamp;
    this.timestamp = searchHistory.created_at;
  }
}
