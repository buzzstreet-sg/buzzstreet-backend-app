import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SearchHistory } from './search.entity';
import { SearchHistoryService } from './search.service';
import { SearchHistoryController } from './search.controller';

@Module({
  imports: [TypeOrmModule.forFeature([SearchHistory])],
  providers: [SearchHistoryService],
  controllers: [SearchHistoryController],
})
export class SearchModule {}
