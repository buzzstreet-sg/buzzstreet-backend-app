import {Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, Index} from 'typeorm';

@Entity('tbl_search_history')
export class SearchHistory {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'search_for_location', nullable: false })
  searchForLocation: string;

  @Column({ name: 'search_for_timestamp', type: 'timestamp', nullable: false })
  searchForTimestamp: Date;

  @Index()
  @CreateDateColumn({ type: 'timestamp', nullable: false })
  created_at: Date;

  constructor(searchForLocation: string, searchForTimestamp: Date) {
    this.searchForLocation = searchForLocation;
    this.searchForTimestamp = searchForTimestamp;
  }

  toString(): string {
    return `SearchHistory { ` +
          `id: ${this.id}, ` +
          `searchForLocation: ${this.searchForLocation}, ` +
          `searchForTimestamp: ${this.searchForTimestamp}, ` +
          `created_at: ${this.created_at} `+
        `}`;
  }
}
