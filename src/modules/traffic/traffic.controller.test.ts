import { Test, TestingModule } from '@nestjs/testing';
import { TrafficController } from './traffic.controller';
import { TrafficService, TrafficCam } from './traffic.service';
import { ApiResponse } from '../../common/response.model';
import { BadRequestException } from '@nestjs/common';

describe('TrafficController', () => {
  let trafficController: TrafficController;
  let trafficService: TrafficService;

  beforeEach(async () => {
    // Mock the TrafficService and its methods
    const trafficServiceMock = {
      findTrafficCamByLocationAndDateTime: jest.fn(),
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [TrafficController],
      providers: [
        {
          provide: TrafficService,
          useValue: trafficServiceMock,
        },
      ],
    }).compile();

    trafficController = module.get<TrafficController>(TrafficController);
    trafficService = module.get<TrafficService>(TrafficService);
  });

  describe('getTrafficCamByLocationAndTimestamp', () => {

    it('should return ApiResponse with traffic camera data for valid location and timestamp', async () => {
      const location = 'location 1';
      const dateTimeStr = '2023-12-16T12:34:56';
      const expectedTrafficCam: TrafficCam = {
        location: 'location 1',
        dateTime: '2023-12-16T12:34:56',
        images: ['image.1.url.png', 'image.2.url.png', 'image.3.url.png']
      };

      jest.spyOn(trafficService, 'findTrafficCamByLocationAndDateTime')
          .mockResolvedValue(expectedTrafficCam);

      const result = await trafficController.getTrafficCamByLocationAndTimestamp(location, dateTimeStr);

      expect(result).toEqual(new ApiResponse<TrafficCam>(expectedTrafficCam));
    });

    it('should throw BadRequestException for an invalid dateTime format', async () => {
      const location = 'example_location';
      const dateTimeStr = 'InvalidDateTime';

      jest.spyOn(trafficService, 'findTrafficCamByLocationAndDateTime')
          .mockRejectedValue(new BadRequestException());

      await expect(trafficController.getTrafficCamByLocationAndTimestamp(location, dateTimeStr)).rejects.toThrowError(
          BadRequestException,
      );
    });

    it('should throw BadRequestException for an empty location', async () => {
      const location = ''; // Empty location
      const dateTimeStr = '2023-12-16T12:34:56';

      jest.spyOn(trafficService, 'findTrafficCamByLocationAndDateTime')
          .mockRejectedValue(new BadRequestException());

      await expect(trafficController.getTrafficCamByLocationAndTimestamp(location, dateTimeStr)).rejects.toThrowError(
          BadRequestException,
      );
    });

  });
});
