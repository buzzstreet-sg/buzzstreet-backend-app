import { BadRequestException, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { RedisService } from "../../common/redis/redis.service";
import { MyLogger} from "../../common/logger/logger.service";
import { roundDownToNearestMinutes } from "../../common/utils/date-utils";

export interface TrafficCam {
  location: string;
  dateTime: string;
  images: string[];
}

@Injectable()
export class TrafficService {
  private readonly logger = new MyLogger(TrafficService.name);

  constructor(
      private readonly redisService: RedisService,
      private readonly configService: ConfigService
  ) {}

  /**
   * Retrieves traffic camera data for a specific location and timestamp.
   *
   * @param {string} location - The location identifier for the traffic camera.
   * @param {string} dateTimeStr - A string representing the date and time.
   * @returns {Promise<TrafficCam>} - The traffic camera data.
   * @throws {BadRequestException} If the date format is invalid.
   */
  async findTrafficCamByLocationAndDateTime(location: string, dateTimeStr: string): Promise<TrafficCam> {

    const strippedLocation =
        location.replace(/\s+/g, '').toLowerCase();
    if (!strippedLocation) {
      throw new BadRequestException(`Location cannot be empty: "${location}".`);
    }

    const dateTime = new Date(dateTimeStr);
    if (isNaN(dateTime.getTime())) {
      throw new BadRequestException(`Invalid date format: ${dateTimeStr}`);
    }

    // Round down the time to search.
    const REDIS_TRAFFIC_INTERVAL_MIN =
        this.configService.get<number>('REDIS_TRAFFIC_INTERVAL_MIN', 30);
    const roundedDateTime = roundDownToNearestMinutes(dateTime, REDIS_TRAFFIC_INTERVAL_MIN);
    this.logger.log(`Converted ${dateTimeStr} to ${roundedDateTime.toISOString()}`);

    const key = `traffic:${strippedLocation}:${roundedDateTime.getTime()}`;
    const value = await this.redisService.getValue(key);

    if (value) {
      this.logger.debug(`${roundedDateTime.toISOString()} returned ${value}.`);
    } else {
      this.logger.log(`No value found for ${roundedDateTime.toISOString()}`);
    }

    return {
      location: location,
      dateTime: dateTimeStr,
      images: JSON.parse(value || '[]'),
    };
  }
}
