import { TrafficService, TrafficCam } from './traffic.service';
import { BadRequestException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import Redis from 'ioredis';
import { RedisService } from '../../common/redis/redis.service';

describe('TrafficService', () => {
  let trafficService: TrafficService;
  let configService: ConfigService;
  let redisService: RedisService;

  beforeEach(() => {
    const redisClient = new Redis();
    redisService = new RedisService(redisClient);
    configService = new ConfigService();
    trafficService = new TrafficService(redisService, configService);
  });

  describe('findTrafficCamByLocationAndDateTime', () => {

    it('should return traffic camera data for a valid location and timestamp', async () => {
      const location = 'Location1';
      const dateTimeStr = '2023-12-16T12:34:56';
      const expectedImages = ['image1.jpg', 'image2.jpg'];

      jest.spyOn(redisService, 'getValue').mockResolvedValue(JSON.stringify(expectedImages));

      const result: TrafficCam = await trafficService.findTrafficCamByLocationAndDateTime(
          location,
          dateTimeStr,
      );

      expect(result).toEqual({
        location: location,
        dateTime: dateTimeStr,
        images: expectedImages,
      });
    });

    it('should return empty images array when Redis value is null', async () => {
      const location = 'Location2';
      const dateTimeStr = '2023-12-16T12:34:56';

      jest.spyOn(redisService, 'getValue').mockResolvedValue(null);

      const result: TrafficCam = await trafficService.findTrafficCamByLocationAndDateTime(
          location,
          dateTimeStr,
      );

      expect(result).toEqual({
        location: location,
        dateTime: dateTimeStr,
        images: [],
      });
    });

    it('should throw BadRequestException for an invalid dateTime format', async () => {
      const location = 'Location3';
      const dateTimeStr = 'InvalidDateTime';

      await expect(
          trafficService.findTrafficCamByLocationAndDateTime(location, dateTimeStr),
      ).rejects.toThrowError(BadRequestException);
    });
  });
});
