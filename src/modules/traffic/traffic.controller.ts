import { Controller, Get, Query, Logger } from '@nestjs/common';
import { TrafficService, TrafficCam } from "./traffic.service";
import { MyLogger } from "../../common/logger/logger.service";
import { ApiResponse } from "../../common/response.model";

@Controller('traffic')
export class TrafficController {
  private readonly logger = new MyLogger(TrafficController.name);

  constructor(private readonly trafficService: TrafficService) {}

  /**
   * Retrieve traffic camera data for a specific location and timestamp.
   *
   * @param {string} location - The location identifier for the traffic camera.
   * @param {string} dateTimeStr - A string representing the date and time.
   * @returns {Promise<ApiResponse<TrafficCam>>} - An ApiResponse containing traffic camera data.
   */
  @Get()
  async getTrafficCamByLocationAndTimestamp(
      @Query('location') location: string,
      @Query('dateTime') dateTimeStr: string
  ): Promise<ApiResponse<TrafficCam>> {
    this.logger.log(`Request received with dateTime: ${dateTimeStr} and location: ${location}.`);

    const trafficCam =
        await this.trafficService.findTrafficCamByLocationAndDateTime(location, dateTimeStr);

    return new ApiResponse<TrafficCam>(trafficCam);
  }
}
