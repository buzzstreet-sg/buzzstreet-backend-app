import Redis from 'ioredis';
import { MyLogger } from '../../common/logger/logger.service';
import { getGeocoding } from "./geocodingUtils";
import { fetchTrafficData, extractLocations as extractTrafficLocations } from '../traffic/trafficUtils';
import { fetchWeatherForecastData, extractLocations as extractWeatherLocations } from '../weather/weatherUtils';


const redis = new Redis({
  host: process.env.REDIS_HOST,
  port: Number(process.env.REDIS_PORT),
});

const logger = new MyLogger('FetchGeocoding');

export const handler = async (): Promise<void> => {
  try {

    const startTime = new Date();
    logger.log(`Fetching traffic cam and weather forecast data at ${startTime.toISOString()}`);

    // Fetch and extract data in parallel
    const [trafficCamData, weatherForecastData] = await Promise.all([
      fetchTrafficData(),
      fetchWeatherForecastData()
    ]);

    logger.log('Fetched and processing retrieved data.');
    const uniqueLocations = new Set<{ latitude: number; longitude: number }>();

    const trafficCamLocations = extractTrafficLocations(trafficCamData);
    trafficCamLocations.forEach(location => uniqueLocations.add(location));

    const weatherForecastLocations = extractWeatherLocations(weatherForecastData);
    weatherForecastLocations.forEach(location => uniqueLocations.add(location));

    // Process Geocoding for Unique Locations
    for (const location of uniqueLocations) {
      const { latitude, longitude } = location;
      const geocodingLocation = await getGeocoding(latitude, longitude);
      logger.debug(`location:${latitude}:${longitude} is ${geocodingLocation}`);

      await redis.set(`location:${latitude}:${longitude}`, geocodingLocation);
    }

    const endTime = new Date();
    const timeTaken = (endTime.getTime() - startTime.getTime()) / 1000;
    logger.log(
        `Location stored in Redis at ${endTime.toISOString()}. Process took: ${timeTaken}`);
  } catch (error) {
    logger.error('Error fetching data:', error);
    throw error;
  }
};

if (!process.env.LAMBDA_RUNTIME_DIR) {
  handler()
  .then(() => {
    logger.log('Data processing complete.');
    process.exit(0);
  })
  .catch((error) => {
    logger.error('Error processing data', error);
    process.exit(1);
  });
}
