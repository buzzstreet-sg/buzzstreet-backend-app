import axios from 'axios';
import { GeocodeResponseData } from "./geocodingTypes";

export const getGeocoding = async (latitude: number, longitude: number): Promise<string> => {
  const url = `https://nominatim.openstreetmap.org/reverse?format=json&lat=${latitude}&lon=${longitude}`;

  try {
    const response = await axios.get<GeocodeResponseData>(url);
    if (response.data && response.data.address) {
      const address = response.data.address;
      const suburb: string = address.suburb;

      if (suburb !== null && suburb !== undefined) {
        return suburb
      } else {
        return address.road;
      }
    } else {
      throw new Error('Geocoding failed');
    }
  } catch (error) {
    console.error('Error in geocoding:', error);
    throw error;
  }
};
