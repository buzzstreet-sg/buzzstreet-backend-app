
export interface Address {
  road: string;
  suburb: string;
}

export interface GeocodeResponseData {
  address: Address;
}