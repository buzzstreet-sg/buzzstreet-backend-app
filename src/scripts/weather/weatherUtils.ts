
import axios from 'axios';
import { WeatherData } from './weatherTypes';

export const fetchWeatherForecastData = async (): Promise<WeatherData> => {
  const response = await axios.get<WeatherData>('https://api.data.gov.sg/v1/environment/2-hour-weather-forecast');
  return response.data;
};


export const extractLocations = (data: WeatherData): Array<{ latitude: number; longitude: number }> => {
  const locations: Array<{ latitude: number; longitude: number }> = [];

  for (const area of data.area_metadata) {
    locations.push({
      latitude: area.label_location.latitude,
      longitude: area.label_location.longitude
    });
  }
  return locations;
};
