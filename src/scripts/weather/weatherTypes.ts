
export interface Forecast {
  area: string;
  forecast: string;
}

export interface WeatherData {
  area_metadata: Array<{
    name: string;
    label_location: {
      latitude: number;
      longitude: number;
    };
  }>;
  items: Array<{
    update_timestamp: string;
    timestamp: string;
    forecasts: Forecast[];
  }>;
}
