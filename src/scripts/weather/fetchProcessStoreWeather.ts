import Redis from 'ioredis';
import { MyLogger } from '../../common/logger/logger.service';
import { WeatherData } from './weatherTypes';
import { fetchWeatherForecastData } from './weatherUtils';
import { roundDownToNearestMinutes } from "../../common/utils/date-utils";

const redis = new Redis({
  host: process.env.REDIS_HOST,
  port: Number(process.env.REDIS_PORT),
});

const logger = new MyLogger('FetchTrafficCam');

export const handler = async (): Promise<void> => {
  try {
    const startTime = new Date();

    logger.log(`Start fetching weather forecast data at ${startTime.toISOString()}`);
    const data: WeatherData = await fetchWeatherForecastData();

    logger.log('Fetched and processing weather forecast data.');
    const processedData =
        await processWeatherForecastData(data);

    logger.log('Push weather forecast data to redis.');
    for (const { key, value } of processedData) {
      await redis.set(key, value, 'EX', process.env.REDIS_WEATHER_TTL_SEC);
    }

    const endTime = new Date();
    const timeTaken = (endTime.getTime() - startTime.getTime()) / 1000;
    logger.log(
        `Weather forecast data fetched and stored in Redis at ${endTime.toISOString()}. ` +
        `Process took: ${timeTaken}`);
  } catch (error) {
    logger.error('Error fetching data:', error);
    throw error;
  }
};


const processWeatherForecastData = async (data: WeatherData): Promise<Array<{ key: string, value: string }>> => {
  const processedData = [];

  const roundedTimestamp =
      roundDownToNearestMinutes(
          new Date(), parseInt(process.env.REDIS_WEATHER_INTERVAL_MIN)).getTime();

  for (const item of data.items) {

    for (const forecast of item.forecasts) {
      const strippedSuburb =
          forecast.area.replace(/\s+/g, '').toLowerCase();

      const key = `weather:${strippedSuburb}:${roundedTimestamp}`;
      const value = forecast.forecast;
      processedData.push({ key, value });
    }

  }

  return processedData;
};

if (!process.env.LAMBDA_RUNTIME_DIR) {
  handler()
  .then(() => {
    logger.log('Data processing complete.');
    process.exit(0);
  })
  .catch((error) => {
    logger.error('Error processing data', error);
    process.exit(1);
  });
}
