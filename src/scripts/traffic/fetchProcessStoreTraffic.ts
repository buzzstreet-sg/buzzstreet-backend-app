import Redis from 'ioredis';
import { MyLogger } from '../../common/logger/logger.service';
import { TrafficData } from './trafficTypes';
import { fetchTrafficData } from './trafficUtils';
import { roundDownToNearestMinutes } from "../../common/utils/date-utils";

const redis = new Redis({
  host: process.env.REDIS_HOST,
  port: Number(process.env.REDIS_PORT),
});

const logger = new MyLogger('FetchTrafficCam');

export const handler = async (): Promise<void> => {
  try {
    const startTime = new Date();

    logger.log(`Start fetching traffic cam data at ${startTime.toISOString()}`);
    const data: TrafficData = await fetchTrafficData();

    logger.log('Fetched and now processing traffic cam data.');
    const {
      timestamp,
      trafficCamMap,
      locations
    } = await processTrafficData(data);

    logger.log('Push traffic cam data to redis.');
    for (const [key, value] of trafficCamMap.entries()) {
      await redis.set(
          key,
          JSON.stringify(value),
          'EX',
          process.env.REDIS_TRAFFIC_TTL_SEC
      );
    }

    logger.log('Push available locations data to redis')
    await redis.set(
        `locations:${timestamp}`,
        JSON.stringify(locations),
        'EX',
        process.env.REDIS_TRAFFIC_TTL_SEC
    );

    const endTime = new Date();
    const timeTaken = (endTime.getTime() - startTime.getTime()) / 1000;
    logger.log(
        `Traffic cam data fetched and stored in Redis at ${endTime.toISOString()}. ` +
        `Process took: ${timeTaken}`
    );
  } catch (error) {
    logger.error('Unexpected error has occurred: ', error);
    throw error;
  }
};

const processTrafficData = async (data: TrafficData): Promise<{
  timestamp: number,
  trafficCamMap: Map<string, string[]>,
  locations: string[]
}> => {
  const trafficCamMap = new Map();
  const locations = [];

  const roundedTimestamp =
      roundDownToNearestMinutes(
          new Date(), parseInt(process.env.REDIS_TRAFFIC_INTERVAL_MIN)).getTime();

  for (const item of data.items) {
    for (const camera of item.cameras) {
      // TODO: Handle case when values is not cached, to manually retrieve the information.
      const locationKey = `location:${camera.location.latitude}:${camera.location.longitude}`;
      const suburb = await redis.get(locationKey);

      if (suburb === null || suburb === undefined || suburb === '') {
        logger.error(`Unable to find suburb for: ${locationKey}`);
        continue;
      }

      const strippedSuburb =
          suburb.replace(/\s+/g, '').toLowerCase();

      const key = `traffic:${strippedSuburb}:${roundedTimestamp}`;
      const value = camera.image;

      // Insert if not update
      if (trafficCamMap.has(key)) {
        trafficCamMap.get(key).push(value);
      } else {
        trafficCamMap.set(key, [value]);
        locations.push(suburb);
      }
    }
  }

  return {
    timestamp: roundedTimestamp,
    trafficCamMap: trafficCamMap,
    locations: locations
  }
};


if (!process.env.LAMBDA_RUNTIME_DIR) {
  handler()
  .then(() => {
    logger.log('Data processing complete.');
    process.exit(0);
  })
  .catch((error) => {
    logger.error('Error processing data', error);
    process.exit(1);
  });
}
