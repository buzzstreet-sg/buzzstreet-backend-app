import axios from 'axios';
import { TrafficData } from './trafficTypes';


export const fetchTrafficData = async (): Promise<TrafficData> => {
  const response = await axios.get<TrafficData>('https://api.data.gov.sg/v1/transport/traffic-images');
  return response.data;
};


export const extractLocations = (data: TrafficData): Array<{ latitude: number; longitude: number }> => {
  const locations: Array<{ latitude: number; longitude: number }> = [];

  for (const item of data.items) {
    for (const camera of item.cameras) {
      locations.push({
        latitude: camera.location.latitude,
        longitude: camera.location.longitude
      });
    }
  }
  return locations;
};
