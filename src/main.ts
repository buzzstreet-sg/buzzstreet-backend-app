import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { MyLogger } from './common/logger/logger.service';
import * as dotenv from 'dotenv';

dotenv.config();

async function bootstrap() {
  const logger = new MyLogger('bootstrap');
  const app = await NestFactory.create(AppModule, {
    logger: logger,
    cors: {
      origin: process.env.BS_FRONTEND_APP_URL,
      methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
      allowedHeaders: 'Content-Type, Accept',
    },
  });

  await app.listen(process.env.PORT || 3001);
  return app.getUrl();
}

bootstrap()
.then((url) => {
  const logger = new MyLogger('bootstrap');
  logger.log(`Application is running on: ${url}`);
})
.catch((error) => {
  const logger = new MyLogger('bootstrap');
  logger.error('Error during application bootstrap', error.stack);
  process.exit(1);
});