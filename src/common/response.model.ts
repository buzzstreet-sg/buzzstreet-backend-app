
export enum ApiResponseStatus {
  Success = 'success',
  Error = 'error'
}

export class ApiResponse<T> {
  status: ApiResponseStatus;
  message?: string;
  data: T;

  constructor(data: T, message: string = '', status: ApiResponseStatus = ApiResponseStatus.Success) {
    this.status = status;
    this.message = message;
    this.data = data;
  }
}
