// logger.module.ts
import { Module, DynamicModule, Global } from '@nestjs/common';
import { MyLogger } from './logger.service';
import { LoggerModuleOptions } from './logger.interface';

@Global()
@Module({})
export class LoggerModule {
  static forRoot(options: LoggerModuleOptions = {}): DynamicModule {
    const providers = [
      {
        provide: MyLogger,
        useValue: new MyLogger(options.context),
      },
    ];

    return {
      module: LoggerModule,
      providers: providers,
      exports: providers,
    };
  }
}
