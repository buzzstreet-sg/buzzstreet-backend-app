import { Injectable } from '@nestjs/common';
import * as winston from 'winston';
import 'winston-daily-rotate-file';

interface LogMessage {
  level: string;
  message: string;
  timestamp: string;
  trace?: string;
}

@Injectable()
export class MyLogger {
  private logger: winston.Logger;

  constructor(private readonly context?: string) {
    this.initializeLogger();
  }

  private initializeLogger() {
    const myFormat = winston.format.printf((info: LogMessage) => {
      const { level, message, timestamp, trace } = info;
      let formattedMessage = `${timestamp} | ${level} | ${this.context} | ${message}`;

      if (trace) {
        formattedMessage += ` | Trace: ${trace}`;
      }

      return formattedMessage;
    });

    this.logger = winston.createLogger({
      level: process.env.LOG_LEVEL || 'info',
      format: winston.format.combine(
          winston.format.timestamp(),
          myFormat
      ),
      transports: [
        new winston.transports.Console({
          format: winston.format.combine(
              winston.format.colorize(),
              winston.format.simple()
          ),
        }),
        // new winston.transports.DailyRotateFile({
        //   filename: 'application-%DATE%.log',
        //   datePattern: 'YYYY-MM-DD',
        //   dirname: process.env.LOG_DIR || 'logs',
        //   maxSize: process.env.LOG_MAX_SIZE || '20m',
        //   maxFiles: process.env.LOG_MAX_FILES || '14d'
        // }),
      ],
    });
  }

  log(message: string) {
    this.logger.info(message);
  }

  error(message: string, trace?: string) {
    const logMessage = trace ? `${message} | Trace: ${trace}` : message;
    this.logger.error(logMessage);
  }

  warn(message: string) {
    this.logger.warn(message);
  }

  debug(message: string) {
    this.logger.debug(message);
  }

  verbose(message: string) {
    this.logger.verbose(message);
  }
}
