
export function roundDownToNearestMinutes(date: Date, interval: number): Date {
  const ms = 1000 * 60 * interval; // convert interval to milliseconds
  const roundedDate = new Date(Math.floor(date.getTime() / ms) * ms);
  return roundedDate;
}
