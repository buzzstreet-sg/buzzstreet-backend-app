import { roundDownToNearestMinutes } from './date-utils';

describe('roundDownToNearestMinutes', () => {
  it('should round down to the nearest minute for interval 1', () => {
    const inputDate = new Date('2023-12-16T12:34:56');
    const interval = 1;
    const expectedDate = new Date('2023-12-16T12:34:00');
    const result = roundDownToNearestMinutes(inputDate, interval);
    expect(result).toEqual(expectedDate);
  });

  it('should round down to the nearest 15 minutes for interval 15', () => {
    const inputDate = new Date('2023-12-16T12:37:22');
    const interval = 15;
    const expectedDate = new Date('2023-12-16T12:30:00');
    const result = roundDownToNearestMinutes(inputDate, interval);
    expect(result).toEqual(expectedDate);
  });

  it('should round down to the nearest 30 minutes for interval 30', () => {
    const inputDate = new Date('2023-12-16T12:45:18');
    const interval = 30;
    const expectedDate = new Date('2023-12-16T12:30:00');
    const result = roundDownToNearestMinutes(inputDate, interval);
    expect(result).toEqual(expectedDate);
  });

  it('should round down to the nearest 60 minutes for interval 60', () => {
    const inputDate = new Date('2023-12-16T13:20:45');
    const interval = 60;
    const expectedDate = new Date('2023-12-16T13:00:00');
    const result = roundDownToNearestMinutes(inputDate, interval);
    expect(result).toEqual(expectedDate);
  });
});
