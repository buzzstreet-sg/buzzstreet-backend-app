import { Injectable, Inject } from '@nestjs/common';
import Redis from 'ioredis';
import { MyLogger } from "../logger/logger.service";

@Injectable()
export class RedisService {
  private logger = new MyLogger(RedisService.name);

  constructor(@Inject('REDIS_CLIENT') private readonly client: Redis) {
    this.client.on('connect', () => this.logger.log('Connected to Redis'));
    this.client.on('error', (err) => this.logger.error('Redis Error', err.stack));
  }

  async setValue(key: string, value: string): Promise<void> {
    await this.client.set(key, value);
  }

  async getValue(key: string): Promise<string> {
    return this.client.get(key);
  }

  async getKeysByPattern(pattern: string): Promise<string[]> {
    let cursor = '0';
    let keys: string[] = [];

    do {
      const reply = await this.client.scan(cursor, 'MATCH', pattern, 'COUNT', 100);
      cursor = reply[0];
      keys.push(...reply[1]);
    } while (cursor !== '0');

    return keys;
  }
}
