import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LoggerModule } from './common/logger/logger.module';
import { RedisModule } from "./common/redis/redis.module";
import { LocationModule } from "./modules/location/location.module";
import { WeatherModule } from "./modules/weather/weather.module";
import { TrafficModule } from "./modules/traffic/traffic.module";
import { SearchModule } from './modules/search/search.module';

@Module({
  imports: [
    RedisModule,
    LocationModule,
    WeatherModule,
    TrafficModule,
    SearchModule,
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    LoggerModule.forRoot({
      context: 'AppModule',
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DB_HOST,
      port: parseInt(process.env.DB_PORT),
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      schema: process.env.DB_SCHEMA,
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: true, // set to false in production
    }),
  ],
  controllers: [],
  providers: [],
  exports: []
})
export class AppModule {}
